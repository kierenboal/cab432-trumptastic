function OnConsolidatedTweets(totalTweets){

	$("#tweets").html("Total Tweets: " + pretty(totalTweets));

}

function OnTweets(tweets){

	/*for (var t in tweets){
		var tweet = tweets[t];
		console.log(tweet.count + " @ " + toDate(tweet.timestamp));
	}*/

}

function OnConsolidatedRetweets(totalRetweets){

	$("#retweets").html("Total Retweets: " + pretty(totalRetweets));

}

function OnRetweets(retweets){

	/*for (var r in retweets){
		var retweet = retweets[r];
		console.log(retweet.count + " @ " + toDate(retweet.timestamp));
	}*/

}

function OnConsolidatedGoodWords(totalGoodWords){

	$("#good").html("Total Good Words: " + pretty(totalGoodWords));

}

function OnGoodWords(goodWords){

	$("#topPositiveTitle").html("Top " + goodWords.length + " positive words");

	var builder = "";

	for (var g in goodWords){
		var word = goodWords[g];
		builder += "<b>" + word.value + "</b> <i>said</i> " + pretty(word.count) + " times<br>";
	}

	$("#topPositive").html(builder);
	
}

function OnConsolidatedBadWords(totalBadWords){

	$("#bad").html("Total Bad Words: " + pretty(totalBadWords));

}

function OnBadWords(badWords){

	$("#topNegativeTitle").html("Top " + badWords.length + " negative words");

	var builder = "";

	for (var b in badWords){
		var word = badWords[b];
		builder += "<b>" + word.value + "</b> <i>said</i> " + pretty(word.count) + " times<br>";
	}

	$("#topNegative").html(builder);

}


function OnConsolidatedAllWords(totalAllWords){

	$("#all").html("Total Overall Words: " + pretty(totalAllWords));

}

function OnAllWords(allWords){
	
	$("#topOverallTitle").html("Top " + allWords.length + " overall words");

	var builder = "";

	for (var a in allWords){
		var word = allWords[a];
		builder += "<b>" + word.value + "</b> <i>said</i> " + pretty(word.count) + " times<br>";
	}

	$("#topOverall").html(builder);

}

function OnConsolidatedRawEmojis(totalRawEmojis){
	
	$("#all").html("Total Emojis: " + pretty(totalRawEmojis));

}

var emojiChart;
function OnRawEmojis(rawEmojis){
	
	$("#topEmojisTitle").html("Top " + rawEmojis.length + " Emojis");

	var builder = "";

	for (var r in rawEmojis){
		var emoji = rawEmojis[r];
		builder += "<b>" + emoji.value + "</b> <i>used</i> " + pretty(emoji.count) + " times<br>";
	}

	$("#topEmojis").html(builder);
	
	var labs = [];
	var ptz = [];
	
	for (var r in rawEmojis){
		var emoji = rawEmojis[r];
		labs.push(emoji.value);
		ptz.push(emoji.count);
	}
	
	$("#emojiChart").html("");

	if (emojiChart != undefined){
		emojiChart.destroy();
	}

	emojiChart = new Chart($("#emojiChart"), {
		type: 'bar',
		data: {
			labels: labs,
			datasets: [{
            label: "Emojis",
            fillColor: "#EDC951",
			fill: true,
			backgroundColor: "rgba(237,201,81,0.4)",
			borderColor: "rgba(237,201,81,1)",
			pointBorderColor: "rgba(237,201,81,1)",
			pointHoverBackgroundColor: "rgba(237,201,81,1)",
			pointHoverBorderColor: "rgba(220,220,220,1)",
			data: ptz
        }]
		},
		options: {
			animation : false,
			maintainAspectRatio: false
		}
	});

}

function OnConsolidatedHashtags(totalHashtags){

	$("#hashtags").html("Total Hashtags: " + pretty(totalHashtags));

}

function OnHashtags(hashtags){
	
	$("#topHashtagsTitle").html("Top #" + hashtags.length + " Hashtags");

	var builder = "";

	for (var h in hashtags){
		var hashtag = hashtags[h];
		builder += "<b>" + hashtag.value + "</b> <i>tagged</i> " + pretty(hashtag.count) + " times<br>";
	}

	$("#topHashtags").html(builder);

}

function OnConsolidatedSources(totalSources){

	$("#sources").html("Total Sources: " + pretty(totalSources));

}

function OnSources(sources){
	
	$("#topSourcesTitle").html("Top " + sources.length + " Tweet Sources");

	var builder = "";

	for (var s in sources){
		var source = sources[s];
		builder += "<b>" + source.value + "</b> <i>posted from</i> " + pretty(source.count) + " times<br>";
	}

	$("#topSources").html(builder);

}

function OnChartData(chartData){

	var margin = {top: 75, right: 100, bottom: 75, left: 100}, width = window.width, height = 300;
	var color = d3.scale.ordinal() .range(["#EDC951"]);

	var radarChartOptions = {
		maxValue: 0.5,
		levels: 5,
		roundStrokes: true,
		color: color
	};

	RadarChart(".radarChart", [chartData], radarChartOptions);

}

var lineChart;
function OnLineChartData(lineData){

	var chartLabels = [];
	var good_data = [];
	var bad_data = [];

	for (var l in lineData) {
		var resp = lineData[l];
		if (resp != undefined) {
			chartLabels.push(toDate(new Date(resp.timestamp)));
			good_data.push(resp.good);
			bad_data.push(resp.bad);

		}
	}

	var chartData = {
		labels: chartLabels,
		datasets: [
			{
				label: "Negative Words",
				fill: true,
				lineTension: 0.1,
				backgroundColor: "rgba(255,0,0,0.4)",
				borderColor: "rgba(255,0,0,1)",
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: "rgba(255,0,0,1)",
				pointBackgroundColor: "#fff",
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: "rgba(255,0,0,1)",
				pointHoverBorderColor: "rgba(220,220,220,1)",
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 10,
				data: bad_data,
				spanGaps: false,
			},
			{
				label: "Positive Words",
				fill: true,
				lineTension: 0.1,
				backgroundColor: "rgba(0,255,0,0.4)",
				borderColor: "rgba(0,255,0,1)",
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: "rgba(0,255,0,1)",
				pointBackgroundColor: "#fff",
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: "rgba(0,255,0,1)",
				pointHoverBorderColor: "rgba(220,220,220,1)",
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 10,
				data: good_data,
				spanGaps: false,
			}

		]
	};

	$("#chart").html("");

	if (lineChart != undefined){
		lineChart.destroy();
	}

	lineChart = new Chart($("#chart"), {
		type: 'line',
		data: chartData,
		options: {
			animation : false,
			maintainAspectRatio: false
		}
	});

}

var tweetChart;
function OnTweetChartData(tweetChartData){
	var chartLabels = [];
	var tweets = [];
	var retweets = [];
	
	for (var t in tweetChartData) {
		var resp = tweetChartData[t];
		if (resp != undefined) {
			chartLabels.push(toDate(new Date(resp.timestamp)));
			tweets.push(resp.tweets);
			retweets.push(resp.retweets);
		}
	}

	var chartData = {
		labels: chartLabels,
		datasets: [
			{
				label: "Retweets",
				fill: true,
				lineTension: 0.1,
				backgroundColor: "rgba(255,0,0,0.4)",
				borderColor: "rgba(255,0,0,1)",
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: "rgba(255,0,0,1)",
				pointBackgroundColor: "#fff",
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: "rgba(255,0,0,1)",
				pointHoverBorderColor: "rgba(220,220,220,1)",
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 10,
				data: retweets,
				spanGaps: false,
			},
			{
				label: "Tweets",
				fill: true,
				lineTension: 0.1,
				backgroundColor: "rgba(0,255,255,0.4)",
				borderColor: "rgba(0,255,255,1)",
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: "rgba(0,255,255,1)",
				pointBackgroundColor: "#fff",
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: "rgba(0,255,255,1)",
				pointHoverBorderColor: "rgba(220,220,220,1)",
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 10,
				data: tweets,
				spanGaps: false,
			},

		]
	};

	$("#tweetChart").html("");

	if (tweetChart != undefined){
		tweetChart.destroy();
	}

	tweetChart = new Chart($("#tweetChart"), {
		type: 'line',
		data: chartData,
		options: {
			animation : false,
			maintainAspectRatio: false
		}
	});
}
