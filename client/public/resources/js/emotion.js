var start, end, milliseconds;

function init(){
	setTimeframe(30 * 60 * 1000);
}

function setTimeframe(ms){
	milliseconds = ms;
	end = new Date();
    $('#end').html("End: <b>" + toDate(end) + "</b>");
	
	start = new Date(new Date() - milliseconds);
    $('#start').html("Start: <b>" + toDate(start) + "</b>");
	
	$('#text').html("Span: <b>" + (milliseconds / (1000 * 60 * 60)) + " hours" + "</b>");
	
	$('#data').html("");
}

function toDate(date){
	
	if (date.getData == undefined){
		date = new Date(date);
	}
	
	return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " " + (date.getHours() > 12 ? date.getHours() - 12 : date.getHours()) + ":" + date.getMinutes() + (date.getHours() > 12 ? "pm" : "am");
}

function pretty(number){
	n = parseFloat(number);
	var n = number.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '1,');
	return n.substring(0, n.indexOf("."));
}

function push(array, obj){
	
	for (var i = 0; i < array.length; i++){
		if (array[i].value == obj.value){
			array[i].count += obj.count;
			return;
		}
	}
	
	array.push(obj);
	
}

function hideEmoji(){
	$("#titleEmoji").fadeOut("fast", function(){
		$("#titleLoader").fadeIn("fast");
	});
}

function showEmoji(){
	$("#titleLoader").fadeOut("fast", function(){
		$("#titleEmoji").fadeIn("fast");
	});
}

function sort(array){
	
	for (var i = 0; i < array.length; i++){
		for (var j = i; j < array.length; j++){
			if (array[i] == undefined || array[j] == undefined){
				continue;
			}
			
			if (array[i].count < array[j].count){
				var temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
			
		}
	}
	
	return array;
	
}

function getData(){
	
	hideEmoji();
	
	$.ajax({
		url: "/data?start=" + (new Date()  - milliseconds - 5000) + "&end=" + (new Date() - 5000),
	}).done(function(data) {
		
		if (data.length == 0) {
			
		} else {

			var tweets = 0;
			var retweets = 0;
			var good_words = 0;
			var bad_words = 0;
			var all_words = 0;
			var raw_emojis = 0;
			var hashtags = 0;
			var sources = 0;
			
			var tweetsArray = [];
			var retweetsArray = [];
			var goodWordsArray = [];
			var badWordsArray = [];
			var allWordsArray = [];
			var rawEmojisArray = [];
			var hashtagsArray = [];
			var sourcesArray = [];
			var chartData = [];
			var lineChartData = [];
			var tweetChartData = [];
			
			for (var d in data) {
				if (data[d] != undefined) {

					var resp = data[d];
					
					tweets += resp.tweets;
					retweets += resp.retweets;
					good_words += resp.positive;
					bad_words += resp.negative;
					
					var tweetsObj = {};
					tweetsObj.count = resp.tweets;
					tweetsObj.timestamp = resp.timestamp;
					tweetsArray.push(tweetsObj);
					
					var retweetsObj = {};
					retweetsObj.count = resp.retweets;
					retweetsObj.timestamp = resp.timestamp;
					retweetsArray.push(retweetsObj);
					
					var lineChartObj = {};
					lineChartObj.good = resp.positive;
					lineChartObj.bad = resp.negative;
					lineChartObj.timestamp = resp.timestamp;
					lineChartData.push(lineChartObj);
					
					var tweetChartObj = {};
					tweetChartObj.tweets = resp.tweets;
					tweetChartObj.retweets = resp.retweets;
					tweetChartObj.timestamp = resp.timestamp;
					tweetChartData.push(tweetChartObj);
					
					for (var i = 0; i < resp.toplists.positive_words.length; i++){
						var word = resp.toplists.positive_words[i];
						var goodObj = {};
						goodObj.value = word.word;
						goodObj.count = word.value;
						goodObj.timestamp = resp.timestamp;
						push(goodWordsArray, goodObj);
					}
					
					for (var i = 0; i < resp.toplists.negative_words.length; i++){
						var word = resp.toplists.negative_words[i];
						var badObj = {};
						badObj.value = word.word;
						badObj.count = word.value;
						badObj.timestamp = resp.timestamp;
						push(badWordsArray, badObj);
					}
					
					for (var i = 0; i < resp.toplists.all_words.length; i++){
						var word = resp.toplists.all_words[i];
						var allObj = {};
						allObj.value = word.word;
						allObj.count = word.value;
						all_words += word.value;
						allObj.timestamp = resp.timestamp;
						push(allWordsArray, allObj);
					}
					
					for (var i = 0; i < resp.toplists.raw_emojis.length; i++){
						var word = resp.toplists.raw_emojis[i];
						var rawObj = {};
						rawObj.value = word.word;
						rawObj.count = word.value;
						raw_emojis += word.value;
						rawObj.timestamp = resp.timestamp;
						push(rawEmojisArray, rawObj);
					}
					
					for (var i = 0; i < resp.toplists.hashtags.length; i++){
						var word = resp.toplists.hashtags[i];
						var hashtagObj = {};
						hashtagObj.value = word.word;
						hashtagObj.count = word.value;
						hashtags += word.value;
						hashtagObj.timestamp = resp.timestamp;
						push(hashtagsArray, hashtagObj);
					}
					
					for (var i = 0; i < resp.toplists.sources.length; i++){
						var word = resp.toplists.sources[i];
						var sourceObj = {};
						sourceObj.value = word.word;
						sourceObj.count = word.value;
						sources += word.value;
						sourceObj.timestamp = resp.timestamp;
						push(sourcesArray, sourceObj);
					}
					
				}
			}
			
			var top_raw = [];
			var best = 0;
			var raw_index = 0;
			var top_raw_count = 0;
			
			for (var i = 0; i < 10; i++){
				
				var best = -1;
				
				for (var r in rawEmojisArray){
					var raw = rawEmojisArray[r];
					
					var isNew = true;
					
					for (var t in top_raw){
						if (top_raw[t].value == raw.value){
							isNew = false;
							break;
						}
					}
					
					if (isNew && raw.count > best){
						best = raw.count;
						raw_index = r;
					}
				}
				
				if (best != -1){
					top_raw_count += rawEmojisArray[raw_index].count;
					top_raw.push(rawEmojisArray[raw_index]);
				}
			}
			
			var biggieSmalls = 0;
			var snoopDogg = 1;
			for (var t in top_raw){
				var raw = top_raw[t];
				var x = raw.count / top_raw_count;
				if (x > biggieSmalls){
					biggieSmalls = x;
				}
				if (x < snoopDogg){
					snoopDogg = x;
				}
			}
			
			for (var t in top_raw){
				var raw = top_raw[t];
				var chartObj = {};
				chartObj.axis = raw.value;
				var x = raw.count / top_raw_count;
				var a = -0.995 * (1 - (snoopDogg / biggieSmalls));
				chartObj.value = (x) / (1 + a * (1 - x));
				
				chartData.push(chartObj);
			}
			
			OnConsolidatedTweets(tweets);
			OnTweets(tweetsArray);
			
			OnConsolidatedRetweets(retweets);
			OnRetweets(retweetsArray);
			
			OnConsolidatedGoodWords(good_words);
			OnGoodWords(sort(goodWordsArray));
			
			OnConsolidatedBadWords(bad_words);
			OnBadWords(sort(badWordsArray));
			
			OnConsolidatedAllWords(all_words);
			OnAllWords(sort(allWordsArray));
			
			OnConsolidatedRawEmojis(raw_emojis);
			OnRawEmojis(sort(rawEmojisArray));
			
			OnConsolidatedHashtags(hashtags);
			OnHashtags(sort(hashtagsArray));
			
			OnConsolidatedSources(sources);
			OnSources(sort(sourcesArray));
			
			OnChartData(chartData);
			OnLineChartData(lineChartData);
			OnTweetChartData(tweetChartData);
			
		}
		
	}).always(function() {
		showEmoji();
		setTimeout(function (){
			getData();
		}, 5000);
	});
}

function datetimeStuff(){
	$(".form_datetime").datetimepicker({
		format: "yyyy-mm-dd hh:mm:ss",
		autoclose: true,
		todayBtn: true,
		minuteStep: 15
	});
	
	var ed = $("#endDate");
	var sd = $("#startDate");
	
	sd.on("change", function(){
		
		if (ed.val() != undefined){
			var edTime = Date.parse(ed.val());
			
			if (edTime < Date.parse(sd.val())){
				sd.val(edTime);
			}
		}
		
	});
	
	ed.on("change", function(){
		alert("HI!");
	});
}

$(document).ready(function(){
	init();
	datetimeStuff();
	getData();
});