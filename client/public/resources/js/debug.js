function send_lag(){
	
	$('#ajax').show();
	
	$.ajax({
		url: "/debug?lag=" + lag,
	}).done(function(data) {
		$('#ajax').fadeOut("fast");
	});
	
	
}


function query(){
	
	$('#ajax').show();

	
	$.ajax({
		url: "/debug",
	}).done(function(data) {
		
		$('#ajax').fadeOut("fast");
		$("#all").show();
		
		var builder = "";
		if (lag == undefined){
			$("#range").val(data.lag);
			Update();
		}
		
		builder += "<hr>";
		builder += "Currently Lag-o-level <b><u>" + data.lag + "</u></b><br>";
		builder += "<hr>";
		builder += "Current CPU: " + pretty(data.load) + "%";
		builder += "<hr>";
		builder += "Current speed: " + pretty(data.speed) + " tweets/hr";
		builder += "<hr>";
		builder += "---Account " + data.creds.access_token_key + "<br>";
		builder += "<hr>";
		builder += "---Total Hosts: " + data.clients + "<br>";
		
		builder += "<br>";
		builder += "The tweets being filtered for this client are: <br>" + data.filters;
		
		
		
		$("#data").html(builder);
		
	}).always(function(){
		setTimeout(function() {
			query();
		}, 500);
	});
	
	
}

function pretty(number){
	var n = number.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '1,');
	return n.substring(0, n.indexOf("."));
}


var lastLag = 0;
var lag;
function Update(){
	
	lag = $("#range").val();
	
	if (lag <= 0){
		$("#lag").html("No lag.");
	} else {
		if (lastLag != lag){
			lastLag = lag;
			$("#lag").html("Lag level <b><u>" + lag + "</u></b>: That's <i>"  + (lag * 1000) + "</i> extra operations PER tweet");
		}
	}
	
	setTimeout(function (){
		Update();
	}, 50);
}

$(document).ready(function(){
	$("#all").hide();
	query();
});