var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
var emojis = [];
var emojiContainer;
var emojiFlowStart = 0;

function MakeEmoji(emoji){
	
	var element = document.createElement("a-emoji");
	element.innerHTML = emoji.word;
	var shadow = document.createElement("a-emoji-shadow");
	element.appendChild(shadow);
	emojiContainer.appendChild(element);
	
	var height = 0;
	if (emoji.value <= 40){
		height = (14 + (3 * emoji.value));
		element.style.fontSize = height + "px";
		emoji.value = -1;
	} else {
		height = 134;
		element.style.fontSize = height + "px";
		emoji.value -= 40;
	}
	
	var yPos = Math.random() * height / 2;
	
	element.style.opacity = 0;
	element.style.zIndex = Math.round(1000 + (yPos * 1000)) + "";
	shadow.style.width = (height * 0.75) + "px";
	shadow.style.height = (height / 6) + "px";
	shadow.style.marginTop = (-height / 6) + "px";
	shadow.style.marginLeft = (height / 4) + "px";
	shadow.style.borderRadius = (height / 8) + "px";
	shadow.style.boxShadow = "0px 0px  " + (height / 4) + "px " + (height / 20) + "px #000";
	
	var newEmoji = new Emoji(element, shadow, height, (Math.random() * (document.documentElement.clientWidth - height)),  yPos);
	emojis.push(newEmoji);
	
}

function Emoji(element, shadow, height, x, y) {
    this.element = element;
	this.shadow = shadow;
	this.height = height;
	this.x = x;
	this.y = y;
	this.floor = y;
	this.oct = 0;
	this.life = 250;
	this.ySpeed = -5;
}
 
Emoji.prototype.update = function () {
	
	this.life--;
	
	//if (Math.random() >= 0.995 && this.ySpeed == 0 && this.life > 0){
	//	this.ySpeed = -5;
	//}
	
	this.ySpeed += 0.4;
	this.y += this.ySpeed;
	
	if (this.oct <= 1 && this.life > 0){
		this.oct += 0.05;
	}
	
	if (this.life <= 0){
		this.oct -= 0.05;
		if (this.oct <= 0){
			return true;
		}
	}
	
	this.element.style.opacity = this.oct;
	
	if (this.y >= this.floor && this.life > 0){
		this.y = this.floor;
		this.ySpeed = 0;
	}
	
	var dy = (this.y - this.floor);
	this.shadow.style.marginTop = Math.round((-this.height / 6) - dy) + "px";
	
    this.element.style["transform"] = "translate3d(" + Math.round(this.x) + "px, " + Math.round(this.y) + "px" + ", 0)";
	 
    return false;
}


function moveEmojis() {
    for (var i = emojis.length - 1; i >= 0; i--) {
        var emoji = emojis[i];
        if (emoji.update()){
			emojiContainer.removeChild(emoji.element);
			emojis.splice(i, 1);
		}
    }
	
    requestAnimationFrame(moveEmojis);
}


function doEmojiFlow(){
	
	var startTime = new Date();
	
	$.ajax({
		url: "/flow"
	}).done(function(data) {
		
		emojiFlowStart = (new Date() * 1);
		RecursiveEmojiMaker(data);
		var next = 10000 - (new Date() - startTime);
		
		setTimeout(function (){
			doEmojiFlow();
		}, next);
	});
}

function RecursiveEmojiMaker(data){
	
	if (data.length <= 1){
		return;
	}
	
	var i = Math.floor((Math.random() * data.length));
	var emoji = data[i];
	
	if (emoji != undefined && emoji != {} && emoji.word != undefined){
		MakeEmoji(emoji);
		
		if (emoji.value <= 0){
			data.splice(i, 1);
		}
	}
	
	var ctr = 1;
	for (var i = 0; i < data.length; i++){
		
		var emoji = data[i];
		if (emoji != undefined && emoji != {} && emoji.word != undefined){
			ctr += Math.ceil(emoji.value / 40);
		}
	}
		
	var next = (10500 - (new Date() - emojiFlowStart)) / (ctr);
	
	setTimeout(function(){
		RecursiveEmojiMaker(data);
	}, next);
}

function setup(){
	document.registerElement('a-emoji');
	document.registerElement('a-emoji-shadow');
	
	emojiContainer = document.createElement("div");
	emojiContainer.id = "emojiContainer";
	
	document.body.appendChild(emojiContainer);
}

$(document).ready(function(){
	setup();
	doEmojiFlow();
	moveEmojis();
});