module.exports = {
	
	init: function(emotions){
		if (this.data != undefined){
			return;
		}
		
		this.innerEmotions = emotions;
		this.clear();
	},
	
	clear: function(){
		this.data = {};
		this.data.tweets = 0;
		this.data.retweets = 0;
		this.data.good_words = 0;
		this.data.bad_words = 0;
		this.data.emotions = {};
		this.data.toplists = {};
		
		for (var i = 0; i < this.innerEmotions.length; i++){
			this.data.emotions[this.innerEmotions[i]] = 0;
		}
		
		this.data.toplists.good_words = [];
		this.data.toplists.bad_words = [];
		this.data.toplists.all_words = [];
		this.data.toplists.raw_emojis = [];
		this.data.toplists.hashtags = [];
		this.data.toplists.sources = [];
	},
	
	add_tweets: function(count){
		this.data.tweets += count;
	},
	
	add_retweets: function(count){
		this.data.retweets += count;
	},
	
	add_emotions: function(emotions){
		for (var i = 0; i < emotions.length && i < 5; i++){
			this.data.emotions[emotions[i].name] += (5 - i);
		}
	},
	
	add_emotion: function(emotion, count){
		this.data.emotions[emotion] += count;
	},
	
	add_good: function(count){
		this.data.good_words += count;
	},
	
	add_bad: function(count){
		this.data.bad_words += count;
	},
	
	add_words: function(words, array){
		for (var i = 0; i < words.length; i++){
			var word = words[i].toLowerCase().trim();
			var isNew = true;
			
			for (var j = 0; j < array.length; j++){
				if (word == array[j].word){
					array[j].value++;
					isNew = false;
					break;
				}
			}
			
			if (isNew){
				var obj = {};
				obj.word = word;
				obj.value = 1;
				array.push(obj);
			}
		}
	},
	
	add_words_filtered: function(words, array){
		for (var i = 0; i < words.length; i++){
			var word = words[i].toLowerCase().trim();
			
			if (word.length <= 5 || word.indexOf("https://") != -1 || word.indexOf("http://") != -1 || word.indexOf("@") != -1 || word.indexOf("#") != -1){
				continue;
			}
			
			var isNew = true;
			
			for (var j = 0; j < array.length; j++){
				if (word == array[j].word){
					array[j].value++;
					isNew = false;
					break;
				}
			}
			
			if (isNew){
				var obj = {};
				obj.word = word;
				obj.value = 1;
				array.push(obj);
			}
		}
	},
	
	add_words_hashtags: function(words, array){
		for (var i = 0; i < words.length; i++){
			var word = words[i].toLowerCase().trim();
			
			if (word.indexOf("#") == -1){
				continue;
			}
			
			var isNew = true;
			
			for (var j = 0; j < array.length; j++){
				if (word == array[j].word){
					array[j].value++;
					isNew = false;
					break;
				}
			}
			
			if (isNew){
				var obj = {};
				obj.word = word;
				obj.value = 1;
				array.push(obj);
			}
		}
	},
	
	get_toplist: function(array){
		var output = [];
	
		for (var j = 0; j < 5; j++){
			var best = 0;
			var bestIndex = 0;
			var bestWord = "";
			
			for (var i = 0; i < array.length; i++){
				if (array[i].value > best){
					best = array[i].value;
					bestWord = array[i].word;
					bestIndex = i;
				}
			}
			
			if (best != 0){
				var obj = {};
				obj.value = best;
				obj.word = bestWord;
				output.push(obj);
				array[bestIndex].value = -1;
			}
		}
		
		return output;
	},
	
	get: function(){
		return this.data;
	}
	
};