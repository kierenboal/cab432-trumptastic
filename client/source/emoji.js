var emojis;
var fs = require('fs');

module.exports = {
	get: function (emoji) {
		this.init();
		return emojis[emoji];
	},
	
	tags: function(){
		this.init();
		
		var data = {};
		
		for (var e in emojis){
			var tags = emojis[e].tags;
			for (var t in tags){
				
				var isNew = true;
				
				for (var d in data){
					if (data[d] === tags[t]){
						isNew = false;
						break;
					}
				}
				
				if (isNew){
					var obj = {};
					obj.emojis = [];
					
					data[emojis[e].tags[t]] = obj;
				}
			}
		}
			
		for (var e in emojis){
			
			var tags = emojis[e].tags;
			for (var t in tags){
				
				var isNew = true;
				
				for (var em in tags[t].emojis){
					if (tags[t].emojis[em] == e){
						isNew = false;
						break;
					}
				}
				
				if (isNew){
					data[tags[t]].emojis.push(e);
				}
			}
		}
		
		return data;
	},
	
	init: function(){
		if (emojis == undefined){
			emojis = JSON.parse(fs.readFileSync('./source/gemoji.json').toString());
		}
	},
	
	parse: function(string){
		var emotions = [];
	
		for (var i = 0; i < string.length - 1; i++){
			
			var c1 = string.charCodeAt(i);
			var c2 = string.charCodeAt(i + 1);
			
			if ((c1 > 54500 && c1 < 58000 && c1 != 65533) && (c2 > 54500 && c2 < 58000 && c2 != 65533)){
				var unicode = toUnicode(string[i] + string[i + 1]).toLowerCase();
				
				var raw = unicode.replace(/\\u([0-9a-f]{4})/g, function (whole, group1) {
					return String.fromCharCode(parseInt(group1, 16));
				});

				var obj = this.get(raw);

				if (obj != undefined && obj.tags != undefined){
					for (var ii = 0; ii < obj.tags.length; ii++){
						var isNew = true;
						
						for (var e in emotions){
							if (e == obj.tags[ii]){
								isNew = false;
								break;
							}
						}
						
						if (isNew){
							var newObj = {};
							newObj.name = obj.tags[ii];
							newObj.value = 1;
							emotions.push(newObj);
						}
					}
				}
			}
		}
		
		return emotions;
	},
	parse_raw: function(string){
		var rawEmojis = [];
	
		for (var i = 0; i < string.length - 1; i++){
			
			var c1 = string.charCodeAt(i);
			var c2 = string.charCodeAt(i + 1);
			
			if ((c1 > 55350 && c1 < 55400 && c1 != 65533) && (c2 > 54500 && c2 < 58000 && c2 != 65533)){
				var unicode = toUnicode(string[i] + string[i + 1]).toLowerCase();
				
				var raw = unicode.replace(/\\u([0-9a-f]{4})/g, function (whole, group1) {
					return String.fromCharCode(parseInt(group1, 16));
				});
			
				rawEmojis.push(raw);
			}
		}
		
		return rawEmojis;
	}
	
};

function toUnicode(theString) {
  var unicodeString = '';
  for (var i=0; i < theString.length; i++) {
    var theUnicode = theString.charCodeAt(i).toString(16).toUpperCase();
    while (theUnicode.length < 4) {
      theUnicode = '0' + theUnicode;
    }
    theUnicode = '\\u' + theUnicode;
    unicodeString += theUnicode;
  }
  return unicodeString;
}