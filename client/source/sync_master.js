var emotions = ["happy", "sad", "angry", "tears", "tear", "joy", "perfect"];

var emoji = require('./emoji');
var request = require('sync-request');
var jsonpack = require('jsonpack/main');
var manager = require('./manager');
var Twitter = require('twitter');
var sentiment = require('sentiment');
var cpu = require('cpu-stats');

module.exports = {
	
	init: function(){
		this.data = {};
		
		this.data.emojiFlow = [];
		this.data.lagFactor = 0;
		this.data.lagFactorTo = 0;
		this.data.cpu = 0;
		this.data.clients = 0;
		this.data.lastCredentials = 0;
		this.data.lastTweet = 0;
		this.data.lastUpdate = new Date();
		this.data.myGUID;
		this.data.lastCreds = "";
		this.data.stream;
		this.data.globalCtr = 0;
		this.data.lastGlobalCtr = 0;
		this.data.lastCtrs = 0;
		this.data.clientFilters;
		this.data.clientCredentials;
		
		manager.init(emotions);
		
		var ref = this;
		setInterval(function (){
			ref.CheckStatus();
		}, 50);
	},
	
	set_database: function(databaseURL){
		this.databaseURL = databaseURL;
	},
	
	set_lag: function(lag){
		if (lag != undefined){
			this.data.lagFactorTo = parseInt(lag);
		}
	},
	
	CheckStatus: function() {	

		if ((new Date() - this.data.lastTweet) > 5000){
			this.data.lastTweet = new Date();
			this.StartStream()
		}

		if ((new Date() - this.data.lastUpdate) > 5000){
			this.data.lastUpdate = new Date();
			this.SyncWithDB();
		}
		
		if ((new Date() - this.data.lastCredentials) > 10000){
			this.data.lastCredentials = new Date();
			this.GetCredentials();
		}
		
	},
	
	GetCredentials: function(){
		var data = {
			guid: this.data.myGUID
		};
		
		try {
			var results = request('POST', this.databaseURL + '/credentials', { json: data });
			var credString = results.getBody('utf8');
			
			if (credString == this.data.lastCreds){
				return;
			} else {
				lastCreds = this.data.credString;
				var creds = jsonpack.unpack(credString);
				this.data.clientCredentials = creds.credentials;
				this.data.clientFilters = creds.filters;
				this.data.clients = creds.clients;
				this.data.myGUID = creds.guid;
				
				this.StartStream();
			}
		} catch (e){
			console.log(e);
			this.data.lastCreds = "";
		}
		
	},
	
	StartStream: function(){
	
		if (this.data.clientCredentials == undefined || this.data.clientFilters == undefined){
			return;
		}
		
		var client = new Twitter({
		  consumer_key: this.data.clientCredentials.consumer_key,
		  consumer_secret: this.data.clientCredentials.consumer_secret,
		  access_token_key: this.data.clientCredentials.access_token_key,
		  access_token_secret: this.data.clientCredentials.access_token_secret
		});
		
		if (this.data.stream != undefined){
			this.data.stream.destroy();
		}
		
		this.data.stream = client.stream('statuses/filter', {track: this.data.clientFilters});

		var ref = this;
		this.data.stream.on('data', function(event) {
			 
			if (event == undefined || event.text == undefined){
				return;
			}
			
			ref.data.lastTweet = new Date();
			
			var reaction = sentiment(event.text);
			
			manager.add_tweets(1);
			manager.add_good(reaction.positive.length);
			manager.add_words(reaction.positive, manager.get().toplists.good_words);
			manager.add_bad(reaction.negative.length);
			manager.add_words(reaction.negative, manager.get().toplists.bad_words);
			manager.add_words_filtered(event.text.split(" "), manager.get().toplists.all_words);
			manager.add_emotions(emoji.parse(event.text));
			manager.add_words(emoji.parse_raw(event.text), manager.get().toplists.raw_emojis);
			manager.add_words(emoji.parse_raw(event.text), ref.data.emojiFlow);
			manager.add_words_hashtags(event.text.split(" "), manager.get().toplists.hashtags);
			
			if (event.source.indexOf("</a>") != -1){
				var rex = /rel="nofollow">(.+)<\/a>/g;
				manager.add_words([rex.exec(event.source)[1]], manager.get().toplists.sources);
			} 

			if (event.text.length >= 4 && event.text.indexOf("RT @") != -1){
				manager.add_retweets(1);
			}
		
			for (var xx = 0; xx < (ref.data.lagFactor * 1000); xx++){
				var w = Math.random();
				var x = Math.random();
				var y = Math.random();
				var z = Math.random();
				z = w;
				y = x;
			}
			
			if (ref.data.lastCtrs < (new Date() - 1000)){
				
				if (ref.data.lagFactorTo > ref.data.lagFactor){
					ref.data.lagFactor += 10;
				} else if (ref.data.lagFactor > ref.data.lagFactorTo){
					ref.data.lagFactor = ref.data.lagFactorTo;
				}
				
				ref.data.lastCtrs = (new Date() * 1);
				ref.data.lastGlobalCtr = ref.data.globalCtr;
				ref.data.globalCtr = 0;
			}
			
			ref.data.globalCtr++;
		});
		
		this.data.stream.on('error', function(error) {
			console.log(error);
		});

		
	},
	
	
	SyncWithDB: function(){
		
		var data = {};
		data.timestamp = (new Date() * 1);
		data.tweets = manager.get().tweets;
		data.retweets = manager.get().retweets;
		data.positive = manager.get().good_words;
		data.negative = manager.get().bad_words;
		data.flow = this.data.emojiFlow;
		data.emotions = manager.get().emotions;
		
		data.toplists = {};
		data.toplists.good_words = manager.get_toplist(manager.get().toplists.good_words);
		data.toplists.bad_words = manager.get_toplist(manager.get().toplists.bad_words);
		data.toplists.all_words = manager.get_toplist(manager.get().toplists.all_words);
		data.toplists.raw_emojis = manager.get_toplist(manager.get().toplists.raw_emojis);
		data.toplists.hashtags = manager.get_toplist(manager.get().toplists.hashtags);
		data.toplists.sources = manager.get_toplist(manager.get().toplists.sources);
		
		manager.clear();
		this.data.emojiFlow = [];
		
		try {
			request("POST", this.databaseURL + '/insert', { json: data });
		} catch (e){
			console.log(e);
		}
	},
	
	debug: function(){
		
		var ref = this;
		cpu(1000, function(error, results) {
			ref.data.cpu = 0;
			for (var i = 0; i < results.length; i++){
				ref.data.cpu += results[i].cpu;
			}
			ref.data.cpu /= results.length;
		});
		
		return {
			creds: this.data.clientCredentials,
			filters: this.data.clientFilters,
			speed: (this.data.lastGlobalCtr * 3600),
			load: this.data.cpu,
			lag: this.data.lagFactor,
			clients: this.data.clients
		};
		
	},
	
	get: function(){
		return this.data;
	}
};















