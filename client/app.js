var databaseURL = "http://localhost:2999";

var express = require('express');
var app = express();
var http = require('http').Server(app);

var request = require('sync-request');
var jsonpack = require('jsonpack/main');
var sync_master = require('./source/sync_master');

app.use("/", express.static(__dirname + '/public'));

app.get('/flow', function(req, res){
		
	try {
		var results = request('GET', databaseURL + '/flow');
		var unpacked = jsonpack.unpack(results.getBody('utf8'));
		res.send(unpacked);
	} catch (e){
		res.send([]);
	}
});

app.get('/data', function(req, res){
	
	var data = {
		end: req.query.end,
		start: req.query.start,
		filter: req.query.filter,
		consolidated: req.query.consolidated
	};
	
	try {
		var results = request('POST', databaseURL + '/data', { json: data });
		var unpacked = jsonpack.unpack(results.getBody('utf8'));
		res.send(unpacked);
	} catch (e){
		res.send([]);
	}

});

app.get('/debug', function(req, res){
	sync_master.set_lag(req.query.lag);
	res.send(sync_master.debug());
});

http.listen(process.argv[2], function(){
	
	if (process.argv[3] != undefined){
		databaseURL = process.argv[3];
	}
	
	sync_master.init();
	sync_master.set_database(databaseURL);
	
});