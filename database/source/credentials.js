var letterFilters = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];
var clients = [];

module.exports = {
	
	init: function () {
		
	},
	
	GetClient: function(guid){
	
		for (var i = 0; i < clients.length; i++){
			if (clients[i].guid == guid){
				return clients[i];
			}
		}
		
		return null;
	},
	
	client_count: function(){
		return clients.length;
	},
	
	purge_old_clients: function(){
		for (var i = 0; i < clients.length; i++){
			var client = clients[i];
			clients[i].position = i;
		}
		
		for (var i = 0; i < clients.length; i++){
			var client = clients[i];
			if (((new Date() * 1) - client.lastActive) >  60000){
				clients.splice(i, 1);
				this.purge_old_clients();
			}
		}
	},
	
	get_credentials: function(guid){
		this.purge_old_clients();
	
		if (this.GetClient(guid) == null){
			
			guid = this.GUID();
			
			clients.push({
				guid: guid,
				position: clients.length,
				lastActive: (new Date() * 1)
			});
		}
		
		var client = this.GetClient(guid);
		client.lastActive = new Date() * 1; 
		
		var step = letterFilters.length / clients.length;
		var start = Math.floor(client.position * step);
		var end = Math.ceil((client.position + 1) * step);
		var letters = [];

		for (var i = start; i <= end; i++){
			if (i >= 0 && i < letterFilters.length){
				letters.push(letterFilters[i]);
			}
		}
	
		return {
			credentials: this.all_credentials()[client.position],
			filters: letters.join(","),
			clients: clients.length,
			guid: client.guid
		};
	},
	
	all_credentials: function(){
		return [
			this.creds_to_obj("zhCcSSVIfHe7mgpfgdxrVclGr FOhSt7812qONqkyXpFzjS6Of3h4vD7MTbj7KSl2P1W1rASPxkJ 791606136512192513-QOJiZoPCbSfmztEgbTuAX1jfj3Osbth 3eY69bloRD5kHPzZfOoWNjzAgmWXyQiCfLYndXjAyxxSd"),
			this.creds_to_obj("gG3GCeM7laOBcPxq291CT8Shg Dzf3gddbMW1WNHPZbYpwIFCt6u8Aj0RrgTtY6O2v01bGySeD7N 3222128036-0DUbObn8rcVPhv9nd1X6OhWmP3V3Sg04IqsLDfz AtD2NgK14zYxt0ULr0nHOTqNcvGFTaoOuEdsOCihjvWBF"),
			this.creds_to_obj("MOhAi1XBOgdUXiXVSw17zhwOX BfJCKX0B11Ti71KEGSSwQTIxsF6p77x918KVvnbJthwoaOvyqf 3222128036-GbHKtH0QsqWstddjZUoyooFJika8fz8JM5JZ3Ik 2VbI1kw1aqprOCrmKsD4r2Whb2k5alm53vuLImN5oTsjc"),
			this.creds_to_obj("ODTKrr7xel4XTKf3XlS9FNzd4 opHKJyyxj7EDR1nDxayAuAdnr7yO2AisIfLVzrHpUgFeIMCKJ2 3222128036-Jsnz7w94tM2JUOmyKidqnlGY2h0NLgLE5q45Pol zyBESTcvyMVwzNakPYKtGEt44cS5PZWUgOw1BHYeOhwpo"),
			this.creds_to_obj("0r0Te9EShjQdhivGTWs6bfwWo ZrFYWRiHtxAxCA01PwADUTzw8LRwpdbnxZGRIVcPgCvFSdbxNE 1192495879-glk2EG0OXY2FRj7W8pgYJz6OUp49I16KAd7hvUF 1n3GE6gUicjFnDprDqGO25M75nb94l9zPSvlvQB7G6j2W"),
			this.creds_to_obj("ndvR72zsBN9RjOEstLiaVj0iS dbfjYeS0VNy0V7G2EOIyZDdNihE6CwHqIIC8XF8zBh0K74dQJh 3222128036-HnC5W8R3rBhAtseHkjIlyyllgOExuK7S2kdMGbZ cwNxqFhjtyex6t8vTU1XZ6dDRp5l8xuw8cH5Pp2GC1SDK"),
			this.creds_to_obj("RA6dULBytCnUi6d48FYBC8s7j wz8rclmKW0W5A33yn4r0qBzfvuVNAkQCQ9kAtetovN7YEbB8Hm 791606136512192513-n83G7MhaR2XKClc91EVDCBA6IrsW8B1 pOSOQcVrQ4rheE9PGvx5l1HImMsF9vjkeAKujjTcIjbfR"),
			this.creds_to_obj("o3tCMN3iiILkBnwG9ylo9fVHL gitRICTTT18rzMv9oCuCt0vbQ3XGwO28zlqlvmc3B5cvx7B7rJ 791606136512192513-xHwg3wCxRXk63yzVCkWJATSc3q6G1rb ktagP5sli3JIveS7OhMGWd0eN2yUnFqNswYfoBCTlZT5j"),
			this.creds_to_obj("YZEmaJcza0jyCqSzJjHdT3myB pVsFlH3E68HGeiV7s2lK0Zc8cHcrN4tf8j2gKyImMZPHZD3kqt 791606136512192513-0BSSxHpbw9DCxnTlWNr6PKKgox7TBLe 9C6ui7ZNsujyOFds6krRuknVjnH7NTXeWYxK3yKV8rhlt"),
			this.creds_to_obj("8q6wgdtkvXQckE0I56kTtkcVF wTZ190SxY4RsJXZhWKV3R69vjJUgUcW1l5ucU419V7rRUJ53Gj 3222128036-6i1GqDxztnSttr7YA19MdUbI2UlThSbdw1EjvkB 3MgICjDXqs80vUK1YAguoTBlWG3oVLQDkYC8jzHwOdN3z"),
			this.creds_to_obj("Fm2FRsHDF3l9tDxV5LQbopx11 j44wFYIBNqiay4Str295bXFpm0sYViHC8j1miXgRE5AQjJQb2T 3222128036-EHgKdwtXfUtTFrEjep2LeBSwOVqETLEZWKB6D9V 2Ou1PDn3Bhs7fi3GMwjUgu8n2vycmd8knZ5k7nMeHDxqN"),
			this.creds_to_obj("Qpg9IHUhB5UPXhwfegnHvo3UR VReFKDopSBxyMiEpah2cFRXloqZTHeoo57MnNNtKglqGhS4vDD 3222128036-SmZx96MPBbf9MLr2sfrpyMqoTzrgOBDMMWWl4u9 s4wPY2WwAliKGqoMLWe4OGL6DLNUszkk9aDqmAZrl7XXe"),
			this.creds_to_obj("E4uGFgRIdG0qjaK4GYHbQs7qv YEjhW8ZCWXGLNGztsN8Rw8RYbB23iYQhRfU8zk5ZV7zFAANpgd 791606136512192513-6j8Fab9mxvQ5m4v1kwB6v1ixhZzETKx YtUJvlWQyIWeGDd4pBINob8qT7hDjdu6IUdJgqsR6ynsy"),
			this.creds_to_obj("Ik1FWk2dpeRJi6C1kpw5HYVTe DabQyw6cwm2XCr39DmE9M0lkKsajHRS3FoOnmYcBmjCnTeDurd 791606136512192513-4uzoqOZ4tZYqGGlSgUztW0Y1jmjWHaT scv3UDgLePGVckmopK7qCg3wG88C6J0FNrRDfKByIgw1K"),
			this.creds_to_obj("U8eFZYcdaYeHzOLhou8NQ1oQD frbbBUzGpYr8wFXEJii4C1UitVFnvvFjaSe6oRaNQ9Yr67rVf5 791606136512192513-spUr59ADrGMT7trCd2hcWrkGyeGgvyE yWDtg9SjTBlQJPqCf3hhxG8Tfisw1SfDusyo0nd2NU3CM")
		];
	},
	
	creds_to_obj: function(string){
		var things = string.split(" ");
		return {
			consumer_key: things[0],
			consumer_secret: things[1],
			access_token_key: things[2],
			access_token_secret: things[3]
		};
	},
	
	GUID: function() {
	  function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	  }
	  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}
	
};