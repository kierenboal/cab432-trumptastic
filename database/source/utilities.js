var emotions = ["happy", "sad", "angry", "tears", "tear", "joy", "perfect"];

module.exports = {
	
	set_timeslice: function (slice){
		this.timeslice = slice;
	},
	timestampUnFix: function(ts){
		return Math.floor(ts * (this.timeslice));
	},

	timestampFix: function(ts){
		return Math.floor(ts / (this.timeslice));
	},

	int: function(num){
		return parseInt(num);
	},

	add: function(num1, num2){
		return this.int(num1) + this.int(num2);
	},
	
	consolidatedRowsToJson: function(rows){
		
		var output = {};
		var allPositiveWords = [];
		var allNegativeWords = [];
		var allAllWords = [];
		var allRawEmojis = [];
		var allHashtags = [];
		var allSources = [];
		var anyEmotes = false;
		
		for (var r in rows){
			
			var row = rows[r];
			if (row.timestamp == null){
				continue;
			}
			
			if (row.tweets != undefined){
				if (output.tweets == undefined){
					output.tweets = 0;
				}
				output.tweets += row.tweets;
			}
			if (row.retweets != undefined){
				if (output.retweets == undefined){
					output.retweets = 0;
				}
				output.retweets += row.retweets;
			}
			if (row.positive != undefined){
				if (output.positive == undefined){
					output.positive = 0;
				}
				output.positive += row.positive;
			}
			if (row.negative != undefined){
				if (output.negative == undefined){
					output.negative = 0;
				}
				output.negative += row.negative;
			}
			
			if (row.positive_words != undefined){
				addWords(JSON.parse(row.positive_words), allPositiveWords);
			}
			
			if (row.negative_words != undefined){
				addWords(JSON.parse(row.negative_words), allNegativeWords);
			}
			
			if (row.all_words != undefined){
				addWords(JSON.parse(row.all_words), allAllWords);
			}
			
			if (row.raw_emojis != undefined){
				addWords(JSON.parse(row.raw_emojis), allRawEmojis);
			}
			
			if (row.hashtags != undefined){
				addWords(JSON.parse(row.hashtags), allHashtags);
			}
			
			if (row.sources != undefined){
				addWords(JSON.parse(row.sources), allSources);
			}
		
			for (var i = 0; i < emotions.length; i++){
				var emotion = emotions[i];
				if (row[emotion] != undefined){
					if (!anyEmotes){
						anyEmotes = true;
						output.emotions = {};
					}
					if (output.emotions[emotion] == undefined){
						output.emotions[emotion] = 0;
					}
					output.emotions[emotion] += row[emotion];
				}
			}
		}
		
		if (allPositiveWords.length != 0 || allNegativeWords.length != 0 || allAllWords.length != 0 || allRawEmojis.length != 0 || allHashtags.length != 0 || allSources.length != 0){
			output.toplists = {};
			
			if (allPositiveWords.length != 0){
				output.toplists.positive_words = toplist(allPositiveWords);
			}
			
			if (allNegativeWords.length != 0){
				output.toplists.negative_words = toplist(allNegativeWords);
			}
			
			if (allAllWords.length != 0){
				output.toplists.all_words = toplist(allAllWords);
			}
			
			if (allRawEmojis.length != 0){
				output.toplists.raw_emojis = toplist(allRawEmojis);
			}
			
			if (allHashtags.length != 0){
				output.toplists.hashtags = toplist(allHashtags);
			}
			
			if (allSources.length != 0){
				output.toplists.sources = toplist(allSources);
			}
		}
		
		return output;
	},

	rowsToJson: function(rows){
		
		var output = [];
		
		for (var r in rows){
			
			var row = rows[r];
			if (row.timestamp == null){
				continue;
			}
				
			var obj = {};
			
			if (row.tweets != undefined){
				obj.tweets = row.tweets;
			}
			if (row.retweets != undefined){
				obj.retweets = row.retweets;
			}
			if (row.positive != undefined){
				obj.positive = row.positive;
			}
			if (row.negative != undefined){
				obj.negative = row.negative;
			}
			
			obj.timestamp = this.timestampUnFix(row.timestamp);
			
			var _toplist = {};
			var anyToplist = false;
			
			if (row.positive_words != undefined){
				_toplist.positive_words = JSON.parse(row.positive_words);
				anyToplist = true;
			}
			
			if (row.negative_words != undefined){
				_toplist.negative_words = JSON.parse(row.negative_words);
				anyToplist = true;
			}
			
			if (row.all_words != undefined){
				_toplist.all_words = JSON.parse(row.all_words);
				anyToplist = true;
			}
			
			if (row.raw_emojis != undefined){
				_toplist.raw_emojis = JSON.parse(row.raw_emojis);
				anyToplist = true;
			}
			
			if (row.hashtags != undefined){
				_toplist.hashtags = JSON.parse(row.hashtags);
				anyToplist = true;
			}
			
			if (row.sources != undefined){
				_toplist.sources = JSON.parse(row.sources);
				anyToplist = true;
			}
			
			if (anyToplist){
				obj.toplists = _toplist;
			}
			
			var _emotion = {};
			var anyEmotion = false;
		
			for (var i = 0; i < emotions.length; i++){
				var emotion = emotions[i];
				if (row[emotion] != undefined){
					_emotion[emotion] = row[emotion];
					anyEmotion = true;
				}
			}
			
			if (anyEmotion){
				obj.emotions = _emotion;
			}
			
			output.push(obj);
		}
		
		return output;
	},
	
	addWords: function(words, array){
		for (var w in words){
			var word = words[w];
			var isNew = true;
			
			for (var j = 0; j < array.length; j++){
				if (word.word == array[j].word){
					array[j].value += word.value;
					isNew = false;
					break;
				}
			}
			
			if (isNew){
				var obj = {};
				obj.word = word.word;
				obj.value = word.value;
				array.push(obj);
			}
		}
	},

	toplist: function(array){
		var data = [];
		
		for (var j = 0; j < 5; j++){
			var best = 0;
			var bestIndex = 0;
			var bestWord = "";
			for (var v in array){
				if (array[v].value > best){
					
					var isNew = true;
					
					for (var d in data){
						if (data[d].word == array[v].word){
							isNew = false;
							break;
						}
					}
					
					if (isNew){
						best = array[v].value;
						bestWord = array[v].word;
						bestIndex = v;
					}
				}
			}
			
			if (best != 0){
				var obj = {};
				obj.value = best;
				obj.word = bestWord;
				data.push(obj);
			}
		}
		
		return data;
	}
	
}