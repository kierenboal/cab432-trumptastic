var emotions = ["happy", "sad", "angry", "tears", "tear", "joy", "perfect"];
var toplists = ["positive_words", "negative_words", "all_words", "raw_emojis", "sources"];
var root_elements = ["tweets", "retweets", "positive", "negative"];
var timeslice = 1000 * 60;

var express = require('express');
var app = express();
var http = require('http').Server(app);

var fs = require("fs");
var sqlite = require('sqlite-sync');
var bodyParser = require('body-parser');
var jsonpack = require('jsonpack/main');
var utilities = require('./source/utilities');
var credentials = require('./source/credentials');

var filename = "database.db";
var good_words = [];
var bad_words = [];
var all_words = [];
var raw_emojis = [];
var hashtags = [];
var sources = [];
var emojiFlow = [];
var oldFrame = 0;
var lastEmojiFlow = 0;

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get('/', function(req, res){
	res.send("<h1>It works!</h1><h3>" + credentials.client_count() + " Clients connected</h3>");
});

app.get('/flow', function(req, res){
	res.send(jsonpack.pack(emojiFlow));
});

app.post('/data', function(req, res){
	
	var post = req.body;
	var timestampStart = utilities.timestampFix(post.start);
	var timestampEnd = utilities.timestampFix(post.end);
	
	if (post.filter == undefined || post.filter == ""){
		var sql = "SELECT * FROM data WHERE (timestamp >= " + timestampStart + " AND timestamp <= " + timestampEnd + ") ORDER BY timestamp DESC;";
	} else {
		
		if (post.filter.indexOf("emotions") != -1){
			post.filter = post.filter.replace("emotions", emotions.join(","));
		}
		
		if (post.filter.indexOf("toplists") != -1){
			post.filter = post.filter.replace("toplists", toplists.join(","));
		}
		
		if (post.filter.indexOf("basic") != -1){
			post.filter = post.filter.replace("basic", root_elements.join(","));
		}
		
		var sql = "SELECT timestamp, " + post.filter.split(",").join(", ") + " FROM data WHERE (timestamp >= " + timestampStart + " AND timestamp <= " + timestampEnd + ") ORDER BY timestamp DESC;";
	}
	
	if (post.consolidated != undefined){
		var json = utilities.consolidatedRowsToJson(sqlite.run(sql));
	} else {
		var json = utilities.rowsToJson(sqlite.run(sql));
	}

	res.send(jsonpack.pack(json));
	
});

app.post('/insert', function(req, res){	

	var post = req.body;
	var timestamp = utilities.timestampFix(post.timestamp);
	
	if (oldFrame != timestamp){
		good_words = [];
		bad_words = [];
		all_words = [];
		raw_emojis = [];
		hashtags = [];
		sources = [];
		oldFrame = timestamp;
	}
	
	utilities.addWords(post.toplists.good_words, good_words);
	utilities.addWords(post.toplists.bad_words, bad_words);
	utilities.addWords(post.toplists.all_words, all_words);
	utilities.addWords(post.toplists.raw_emojis, raw_emojis);
	utilities.addWords(post.toplists.hashtags, hashtags);
	utilities.addWords(post.toplists.sources, sources);
	
	if (lastEmojiFlow < (new Date() - 10000)){
		lastEmojiFlow = new Date();
		emojiFlow = [];
	}
	utilities.addWords(post.flow, emojiFlow);
	
	var sql1 = "SELECT * FROM data WHERE timestamp = " + timestamp + ";";
	var rows = sqlite.run(sql1);

	if (rows.length == 0){
		console.log("Recieved data... " + timestamp + " new dataset created.");
		
		var sql2 = "INSERT INTO data VALUES (null," + utilities.int(post.tweets) + "," + utilities.int(post.retweets) + "," + utilities.int(post.positive) + "," + utilities.int(post.negative) + "," + timestamp + ",'" + JSON.stringify(utilities.toplist(sources)) + "','" + JSON.stringify(utilities.toplist(good_words)) + "','" + JSON.stringify(utilities.toplist(bad_words)) + "','" + JSON.stringify(utilities.toplist(all_words)) + "','" + JSON.stringify(utilities.toplist(raw_emojis)) + "'" + ",'" + JSON.stringify(utilities.toplist(hashtags)) + "'";
		for (var i = 0; i < emotions.length; i++){
			sql2 += "," + utilities.int(post.emotions[emotions[i]]);
		}
		sql2 += ");";
	} else {
		console.log("Recieved data... " + timestamp + " dataset updated.");
		var row = rows[0];

		var sql2 = "UPDATE data SET tweets = " + utilities.add(post.tweets, row.tweets) + ", retweets = " + utilities.add(post.retweets, row.retweets) + ", positive = " + utilities.add(post.positive, row.positive) + ", negative = " + utilities.add(post.negative, row.negative) + ", positive_words = '" + JSON.stringify(utilities.toplist(good_words))  + "', negative_words = '" + JSON.stringify(utilities.toplist(bad_words)) + "', all_words = '" + JSON.stringify(utilities.toplist(all_words)) + "', raw_emojis = '" + JSON.stringify(utilities.toplist(raw_emojis))  + "', sources = '" + JSON.stringify(utilities.toplist(sources)) + "', hashtags = '" + JSON.stringify(utilities.toplist(hashtags)) + "'";
		for (var i = 0; i < emotions.length; i++){
			var emotion = emotions[i];
			sql2 += ", " + emotion + " = " + utilities.add(post.emotions[emotion], row[emotion]);
		}
		sql2 += " WHERE timestamp = " + timestamp + ";";
	}
	
	sqlite.run(sql2);
	res.send(":-)");
});

http.listen(2999, function(){
	utilities.set_timeslice(timeslice);
	SetupDatabase();
});

app.post('/credentials', function(req, res){
	var json = credentials.get_credentials(req.body.guid);
	res.send(jsonpack.pack(json));
});

function SetupDatabase(){
	
	if (fs.existsSync(filename)){
		sqlite.connect(filename);
		return;
	}
	
	sqlite.connect(filename);
	var sql = "CREATE TABLE data (id integer PRIMARY KEY AUTOINCREMENT, tweets integer, retweets integer, positive integer, negative integer, timestamp integer, sources text, positive_words text, negative_words text, all_words text, raw_emojis text, hashtags text";
	for (var i = 0; i < emotions.length; i++){
		sql += ", " + emotions[i] + " integer";
	}
	sql += ");";
	
	sqlite.run(sql);
	
}
